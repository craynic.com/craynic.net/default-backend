# 1. Building assets

# don't use the Alpine version here - it does not contain Python
FROM node:17 AS assets-builder

WORKDIR /opt/app

COPY /app/assets ./assets/
COPY /app/package.* ./
COPY /app/webpack.config.js ./
COPY /app/yarn.lock ./

RUN mkdir ./public \
    && yarn install \
    && yarn run encore production

# 2. Composer

FROM composer:2 AS vendor-builder

WORKDIR /opt/app

COPY /app/composer.* /opt/app/

RUN composer --no-dev --ignore-platform-reqs install

# 3. The real shit

FROM registry.gitlab.com/craynic.com/docker/lap:9

WORKDIR /var/www/html/

COPY /app/config ./config/
COPY /app/public ./public/
COPY /app/src ./src/
COPY /app/templates ./templates/
COPY /app/.htaccess ./
COPY /app/bootstrap.php ./

COPY --from=assets-builder /opt/app/public/assets/ ./public/assets/
COPY --from=vendor-builder /opt/app/vendor/ ./vendor/

RUN chmod -R u+rwX,g-w,g+rX,o-rwx /var/www/html/ \
    && chown -R www-data:www-data /var/www/html/
