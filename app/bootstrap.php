<?php
declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

// get app mode
define('APP_ENV', getenv('APP_ENV') ?: 'prod');
const APP_IS_DEV = APP_ENV === 'dev';
