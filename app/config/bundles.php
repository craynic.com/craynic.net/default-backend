<?php

declare(strict_types=1);

use App\AppBundle\AppBundle;
use Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\WebpackEncoreBundle\WebpackEncoreBundle;

return [
    FrameworkBundle::class => ['all' => true],
    TwigBundle::class => ['all' => true],
    AppBundle::class => ['all' => true],
    WebpackEncoreBundle::class => ['all' => true],
    SensioFrameworkExtraBundle::class => ['all' => true],
];
