<?php
declare(strict_types=1);

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator, ContainerBuilder $container): void {
    $container->loadFromExtension(
        'framework',
        [
            'router' => [
                'resource' => '%kernel.project_dir%/config/routing.php',
                'strict_requirements' => true,
            ],

            'assets' => [
                'packages' => [
                    'encore' => [
                        'json_manifest_path' => '%kernel.project_dir%/public/assets/manifest.json',
                    ],
                ],
            ],

            'secret' => '%env(APP_SECRET)%',
        ],
    );

    $container->loadFromExtension(
        'webpack_encore',
        [
            'output_path' => '%kernel.project_dir%/public/assets',
            'crossorigin' => 'anonymous',
            'strict_mode' => true,
        ]
    );

    $container->setParameter('app.healthz_name', '%env(APP_HEALTHZ_NAME)%');

    $services = $configurator->services()->defaults()
        ->autowire()
        ->autoconfigure();

    $services->load('App\\', '../src/*')
        ->exclude('../src/{AppBundle/Entity,Kernel.php}');
};
