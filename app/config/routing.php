<?php

declare(strict_types=1);

// config/routes.php
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->import('@AppBundle/Controller', 'annotation');
};
