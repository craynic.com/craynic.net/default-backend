<?php

declare(strict_types=1);

namespace App\AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class AppBundle extends Bundle
{
}
