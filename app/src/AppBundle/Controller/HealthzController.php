<?php

/** @noinspection PhpUnused */
declare(strict_types=1);

namespace App\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HealthzController extends AbstractController
{
    /**
     * @Route("/healthz", methods={"GET","HEAD"})
     */
    public function index(): Response
    {
        return new Response(sprintf(
            '%s OK',
            $this->getParameter('app.healthz_name')
        ));
    }
}
