<?php

/** @noinspection PhpUnused */
declare(strict_types=1);

namespace App\AppBundle\Controller;

use App\AppBundle\Service\AcceptLanguageParser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    private AcceptLanguageParser $acceptLanguageParser;

    public function __construct(AcceptLanguageParser $acceptLanguageParser)
    {
        $this->acceptLanguageParser = $acceptLanguageParser;
    }

    /**
     * @Route("/", methods={"GET","HEAD"})
     */
    public function index(Request $request): Response
    {
        $language = $this->acceptLanguageParser->getBestLanguage(
            $request->headers->get('accept-language'),
            ['cs', 'en']
        );

        return $this->render(
            sprintf('@App/pages/notfound-%s.twig', $language),
        );
    }
}
