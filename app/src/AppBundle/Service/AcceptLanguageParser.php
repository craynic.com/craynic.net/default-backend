<?php

declare(strict_types=1);

namespace App\AppBundle\Service;

final class AcceptLanguageParser
{
    public function getBestLanguage(string $headerValue, array $priorities): string
    {
        if ($headerValue !== '*') {
            // parse languages
            $languages = $this->parseLanguages($headerValue);

            foreach ($languages as $language) {
                if (in_array($language, $priorities)) {
                    return $language;
                }
            }
        }

        return $priorities[0];
    }

    private function parseLanguages(string $headerValue): array
    {
        $languages = [];
        foreach (explode(',', $headerValue) as $headerPart) {
            if (strpos($headerPart, ';') !== false) {
                [$lang, $priority] = explode(';', $headerPart, 2);
            } else {
                $lang = $headerPart;
                $priority = 'q=1';
            }

            $priority = (substr($priority, 0, 2) === 'q=')
                ? (float)substr($priority, 2)
                : 1;

            $languages[] = [explode('-', $lang, 2)[0], $priority];
        }

        usort($languages, fn(array $a, array $b): int => ($a[1] === $b[1]) ? -1 : $b[1] <=> $a[1]);

        return array_map(fn(array $languageData): string => $languageData[0], $languages);
    }
}
