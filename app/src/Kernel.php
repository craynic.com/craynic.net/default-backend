<?php

declare(strict_types=1);

namespace App;

use Symfony\Component\Config\Loader\LoaderInterface;

final class Kernel extends \Symfony\Component\HttpKernel\Kernel
{
    public function registerBundles()
    {
        $contents = require __DIR__ . '/../config/bundles.php';

        foreach ($contents as $class => $envs) {
            if ($envs[$this->environment] ?? $envs['all'] ?? false) {
                yield new $class();
            }
        }
    }

    public function getProjectDir(): string
    {
        return __DIR__ . '/..';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(
            sprintf('%s/config/config_%s.php', $this->getProjectDir(), $this->getEnvironment())
        );
    }

    public function getCacheDir()
    {
        return sys_get_temp_dir() . '/cache/';
    }

    public function getLogDir()
    {
        return sys_get_temp_dir() . '/log/';
    }
}
